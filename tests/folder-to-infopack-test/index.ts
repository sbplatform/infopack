import { Executor, InfopackContentInput, PipelineStep } from '../../lib/pipeline'
import Pipeline from '../../index'
import { step as folderToInfopack } from '../../lib/generators/folder-to-infopack'

const run = function (executor: Executor) {
  executor.toOutput({
    path: 'zzz.snippet.html',
    title: 'zzz Snippet',
    description: 'Some fancy description',
    data: Buffer.from('<h1>En rubrik</h1><p>Hej detta är en mening</p>')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'zzz/zzz.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'test/aaa.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'aaa.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'test-mapp/aaa.txt',
    title: 'A som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'test-mapp/bbb.txt',
    title: 'B som i alfabetet',
    description: 'Some fancy description',
    data: Buffer.from('some data')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'origin-test/en-markdown-fil.md',
    title: 'Ett exempel på en markdown-fil',
    description: 'Detta är ett en exempel på en markdown fil',
    data: Buffer.from('# Hello')
  } as InfopackContentInput)

  executor.toOutput({
    path: 'origin-test/en-html-fil.partial.html',
    title: 'Ett exempel på en markdown-fil',
    description: 'Detta är ett en exempel på en html fil som är genererad från en html fil',
    origin: ['./en-markdown-fil.md'],
    data: Buffer.from('<h1>Hello</h1>')
  } as InfopackContentInput)

  return Promise.resolve()
}

const steps: PipelineStep[] = [
  folderToInfopack(),
  new PipelineStep(run)
]

const pipeline = new Pipeline(steps, {
  namespace: 'swe-nrb',
  title: 'Testpaket',
  basePath: 'tests/folder-to-infopack-test/test_dir'
})

pipeline.run()
